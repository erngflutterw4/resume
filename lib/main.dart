import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget textName = Container(
        child: Column(children: [
      Text(
        'Ornnikarn Napachot',
        style: TextStyle(
          color: Colors.grey[900],
          fontWeight: FontWeight.bold,
          fontSize: 18,
        ),
      ),
      Text(
        'Programmer',
        style: TextStyle(
          color: Colors.grey[900],
          fontWeight: FontWeight.bold,
          fontSize: 16,
        ),
      ),
    ]));

    Widget myimage = Container(
        padding: const EdgeInsets.all(32),
        child: Image.asset(
          'images/erng.jpg',
          width: 600,
          height: 240,
        ));

    Widget profileBar = Container(
      margin: const EdgeInsets.fromLTRB(10, 32, 10, 32),
      child: Column(children: [
        Container(
          color: Colors.blue[200],
          height: 40,
          width: 1600,
          padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
          child: Row(
            children: [
              Icon(
                Icons.person,
                size: 20.0,
                color: Colors.blueGrey[600],
              ),
              Text(' Profile',
                  style: TextStyle(
                    color: Colors.grey[900],
                    fontWeight: FontWeight.bold,
                    fontSize: 16,
                  ))
            ],
          ),
        ),
      ]),
    );

    Widget profileText = Container(
      padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
      child: Text(
        'I am studying at Burapha University. '
        'Faculty of Information Science, Computer Science. '
        'I have learned about programming, databases, UX/UI design, and so much more. '
        'I look forward to bringing my knowledge and experience to that I have to use for work. ',
        softWrap: true,
      ),
    );

    Widget personalBar = Container(
      margin: const EdgeInsets.fromLTRB(10, 32, 10, 32),
      child: Column(children: [
        Container(
          color: Colors.blue[200],
          height: 40,
          width: 1600,
          padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
          child: Row(
            children: [
              Icon(
                Icons.info,
                size: 20.0,
                color: Colors.blueGrey[600],
              ),
              Text(' Personal Information',
                  style: TextStyle(
                    color: Colors.grey[900],
                    fontWeight: FontWeight.bold,
                    fontSize: 16,
                  ))
            ],
          ),
        ),
      ]),
    );

    Widget personalText = Container(
        padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Text(
            'Gender: Female',
            style: TextStyle(
              color: Colors.grey[900],
              fontSize: 14,
            ),
          ),
          Text(
            'Date of birth: July 31, 1999',
            style: TextStyle(
              color: Colors.grey[900],
              fontSize: 14,
            ),
          ),
          Text(
            'Nationality: Thai',
            style: TextStyle(
              color: Colors.grey[900],
              fontSize: 14,
            ),
          ),
          Text(
            'Marital Status: Single',
            style: TextStyle(
              color: Colors.grey[900],
              fontSize: 14,
            ),
          ),
        ]));

    Widget eduBar = Container(
      margin: const EdgeInsets.fromLTRB(10, 32, 10, 32),
      child: Column(children: [
        Container(
          color: Colors.blue[200],
          height: 40,
          width: 1600,
          padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
          child: Row(
            children: [
              Icon(
                Icons.school,
                size: 20.0,
                color: Colors.blueGrey[600],
              ),
              Text('  Education',
                  style: TextStyle(
                    color: Colors.grey[900],
                    fontWeight: FontWeight.bold,
                    fontSize: 16,
                  ))
            ],
          ),
        ),
      ]),
    );

    Widget eduText = Container(
        padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
        child: Column(
          children: [
            Row(
              children: [
                Container(
                    margin: const EdgeInsets.fromLTRB(0, 0, 30, 0),
                    height: 50,
                    width: 100,
                    child: Text('2018 - Present')),
                Container(
                  height: 50,
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Computer Science – in progress '),
                        Text('Faculty of Information Science,'),
                        Text('Burapha University,')
                      ]),
                )
              ],
            ),
            Row(
              children: [
                Container(
                    margin: const EdgeInsets.fromLTRB(0, 0, 30, 0),
                    height: 50,
                    width: 100,
                    child: Text('2016 - 2018')),
                Container(
                    height: 50,
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('High school certificate'),
                          Text('ChonradsadornumrungSchool')
                        ]))
              ],
            ),
          ],
        ));

    Widget skillsBar = Container(
      margin: const EdgeInsets.fromLTRB(10, 32, 10, 32),
      child: Column(children: [
        Container(
          color: Colors.blue[200],
          height: 40,
          width: 1600,
          padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
          child: Row(
            children: [
              Icon(
                Icons.lightbulb,
                size: 20.0,
                color: Colors.blueGrey[600],
              ),
              Text(' Skills',
                  style: TextStyle(
                    color: Colors.grey[900],
                    fontWeight: FontWeight.bold,
                    fontSize: 16,
                  ))
            ],
          ),
        ),
      ]),
    );

    Widget skillsSection = Container(
        child: Column(
      children: [
        Row(
          children: [
            Container(
              margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
              child: Image.asset(
                'images/html.png',
                width: 40,
                height: 40,
              ),
            ),
            Container(
              margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
              child: Image.asset(
                'images/css.png',
                width: 40,
                height: 40,
              ),
            ),
            Container(
              margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
              child: Image.asset(
                'images/js.png',
                width: 40,
                height: 40,
              ),
            ),
            Container(
              margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
              child: Image.asset(
                'images/vue.png',
                width: 40,
                height: 40,
              ),
            ),
            Container(
              margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
              child: Image.asset(
                'images/flutter.png',
                width: 40,
                height: 40,
              ),
            )
          ],
        ),
        Row(
          children: [
            Container(
              margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
              child: Image.asset(
                'images/vscode.png',
                width: 40,
                height: 40,
              ),
            ),
            Container(
              margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
              child: Image.asset(
                'images/netbeans.png',
                width: 40,
                height: 40,
              ),
            ),
            Container(
              margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
              child: Image.asset(
                'images/eclipse.png',
                width: 40,
                height: 40,
              ),
            ),
            Container(
              margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
              child: Image.asset(
                'images/java.png',
                width: 40,
                height: 40,
              ),
            )
          ],
        )
      ],
    ));

    return MaterialApp(
        title: 'resume layout demo',
        home: Scaffold(
          appBar: AppBar(
            title: const Text('Resume'),
          ),
          body: ListView(children: [
            myimage,
            textName,
            profileBar,
            profileText,
            personalBar,
            personalText,
            eduBar,
            eduText,
            skillsBar,
            skillsSection
          ]),
        ));
  }
}
